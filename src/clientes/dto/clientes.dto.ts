export class ClientesDto {
  readonly nome_completo: string;
  readonly dt_nascimento: string;
  readonly sexo: string;
  readonly email?: string;
  readonly celular?: string;
  readonly telefone?: string;
  readonly empresa_id: string;
}
