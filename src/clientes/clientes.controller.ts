import Aigle from "aigle";

import { PinoLogger } from "nestjs-pino";
import { Controller, Inject } from "@nestjs/common";
import { GrpcMethod } from "@nestjs/microservices";
import { isEmpty, isNil } from "lodash";

import { ICount, IQuery } from "../commons/commons.interface";
import { IClientesService } from "./interfaces/clientes.interface";
import { IFindPayload } from "../commons/cursor-pagination.interface";

import { Clientes } from "./clientes.model";
import { ClientesDto } from "./dto/clientes.dto";

const { map } = Aigle;

@Controller()
export class ClientesController {
  constructor(
    @Inject("ClientesService")
    private readonly service: IClientesService,
    private readonly logger: PinoLogger
  ) {
    logger.setContext(ClientesController.name);
  }

  @GrpcMethod("ClientesService", "find")
  async find(query: IQuery): Promise<IFindPayload<Clientes>> {
    this.logger.info("ClientesController#findAll.call %o", query);

    const { results, cursors } = await this.service.find({
      attributes: !isEmpty(query.select)
        ? ["id"].concat(query.select)
        : undefined,
      where: !isEmpty(query.where) ? JSON.parse(query.where) : undefined,
      order: !isEmpty(query.orderBy) ? query.orderBy : undefined,
      limit: !isNil(query.limit) ? query.limit : 25,
      before: !isEmpty(query.before) ? query.before : undefined,
      after: !isEmpty(query.after) ? query.after : undefined,
    });

    const result: IFindPayload<Clientes> = {
      edges: await map(results, async (comment: Clientes) => ({
        node: comment,
        cursor: Buffer.from(JSON.stringify([comment.id])).toString("base64"),
      })),
      pageInfo: {
        startCursor: cursors.before || "",
        endCursor: cursors.after || "",
        hasNextPage: cursors.hasNext || false,
        hasPreviousPage: cursors.hasPrevious || false,
      },
    };

    this.logger.info("ClientesController#findAll.result %o", result);

    return result;
  }

  @GrpcMethod("ClientesService", "findById")
  async findById({ id }): Promise<Clientes> {
    this.logger.info("ClientesController#findById.call %o", id);

    const result: Clientes = await this.service.findById(id);

    this.logger.info("ClientesController#findById.result %o", result);

    if (isEmpty(result)) throw new Error("Record not found.");

    return result;
  }

  @GrpcMethod("ClientesService", "findOne")
  async findOne(query: IQuery): Promise<Clientes> {
    this.logger.info("ClientesController#findOne.call %o", query);

    const result: Clientes = await this.service.findOne({
      attributes: !isEmpty(query.select) ? query.select : undefined,
      where: !isEmpty(query.where) ? JSON.parse(query.where) : undefined,
    });

    this.logger.info("ClientesController#findOne.result %o", result);

    if (isEmpty(result)) throw new Error("Record not found.");

    return result;
  }

  @GrpcMethod("ClientesService", "count")
  async count(query: IQuery): Promise<ICount> {
    this.logger.info("ClientesController#count.call %o", query);

    const count: number = await this.service.count({
      where: !isEmpty(query.where) ? JSON.parse(query.where) : undefined,
    });

    this.logger.info("ClientesController#count.result %o", count);

    return { count };
  }

  @GrpcMethod("ClientesService", "create")
  async create(data: ClientesDto): Promise<Clientes> {
    this.logger.info("ClientesController#create.call %o", data);

    const result: Clientes = await this.service.create(data);

    this.logger.info("ClientesController#create.result %o", result);

    return result;
  }

  @GrpcMethod("ClientesService", "update")
  async update({ id, data }): Promise<Clientes> {
    this.logger.info("ClientesController#update.call %o %o", id, data);

    const result: Clientes = await this.service.update(id, data);

    this.logger.info("ClientesController#update.result %o", result);

    return result;
  }

  @GrpcMethod("ClientesService", "destroy")
  async destroy(query: IQuery): Promise<ICount> {
    this.logger.info("ClientesController#destroy.call %o", query);

    const count: number = await this.service.destroy({
      where: !isEmpty(query.where) ? JSON.parse(query.where) : undefined,
    });

    this.logger.info("ClientesController#destroy.result %o", count);

    return { count };
  }
}
