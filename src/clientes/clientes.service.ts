import { isEmpty } from "lodash";
import { PinoLogger } from "nestjs-pino";
import { Injectable } from "@nestjs/common";
import { FindOptions } from "sequelize/types";
import { InjectModel } from "@nestjs/sequelize";

import { IClientesService } from "./interfaces/clientes.interface";
import {
  IFindAndPaginateOptions,
  IFindAndPaginateResult,
} from "../commons/find-and-paginate.interface";

import { Clientes } from "./clientes.model";
import { ClientesDto } from "./dto/clientes.dto";

@Injectable()
export class ClientesService implements IClientesService {
  constructor(
    @InjectModel(Clientes) private readonly repo: typeof Clientes,
    private readonly logger: PinoLogger
  ) {
    logger.setContext(ClientesService.name);
  }

  async find(
    query?: IFindAndPaginateOptions
  ): Promise<IFindAndPaginateResult<Clientes>> {
    this.logger.info("ClientesService#findAll.call %o", query);

    const result: IFindAndPaginateResult<Clientes> =
      //@ts-ignore
      await this.repo.findAndPaginate({
        ...query,
        raw: true,
        paranoid: false,
      });

    this.logger.info("ClientesService#findAll.result %o", result);

    return result;
  }

  async findById(id: string): Promise<Clientes> {
    this.logger.info("ClientesService#findById.call %o", id);

    const result: Clientes = await this.repo.findByPk(id, {
      raw: true,
    });

    this.logger.info("ClientesService#findById.result %o", result);

    return result;
  }

  async findOne(query: FindOptions): Promise<Clientes> {
    this.logger.info("ClientesService#findOne.call %o", query);

    const result: Clientes = await this.repo.findOne({
      ...query,
      raw: true,
    });

    this.logger.info("ClientesService#findOne.result %o", result);

    return result;
  }

  async count(query?: FindOptions): Promise<number> {
    this.logger.info("ClientesService#count.call %o", query);

    const result: number = await this.repo.count(query);

    this.logger.info("ClientesService#count.result %o", result);

    return result;
  }

  async create(user: ClientesDto): Promise<Clientes> {
    this.logger.info("ClientesService#create.call %o", user);

    const result: Clientes = await this.repo.create(user);

    this.logger.info("ClientesService#create.result %o", result);

    return result;
  }

  async update(id: string, user: ClientesDto): Promise<Clientes> {
    this.logger.info("ClientesService#update.call %o", user);

    const record: Clientes = await this.repo.findByPk(id);

    if (isEmpty(record)) throw new Error("Record not found.");

    const result: Clientes = await record.update(user);

    this.logger.info("ClientesService#update.result %o", result);

    return result;
  }

  async destroy(query?: FindOptions): Promise<number> {
    this.logger.info("ClientesService#destroy.call %o", query);

    const result: number = await this.repo.destroy(query);

    this.logger.info("ClientesService#destroy.result %o", result);

    return result;
  }
}
