import { FindOptions } from "sequelize/types";

import { Clientes } from "../clientes.model";
import { ClientesDto } from "../dto/clientes.dto";
import {
  IFindAndPaginateOptions,
  IFindAndPaginateResult,
} from "../../commons/find-and-paginate.interface";

export interface IClientesService {
  find(
    query?: IFindAndPaginateOptions
  ): Promise<IFindAndPaginateResult<Clientes>>;
  findById(id: string): Promise<Clientes>;
  findOne(query?: FindOptions): Promise<Clientes>;
  count(query?: FindOptions): Promise<number>;
  create(comment: ClientesDto): Promise<Clientes>;
  update(id: string, comment: ClientesDto): Promise<Clientes>;
  destroy(query?: FindOptions): Promise<number>;
}
