import { Module } from "@nestjs/common";
import { LoggerModule } from "nestjs-pino";
import { SequelizeModule } from "@nestjs/sequelize";

import { Clientes } from "./clientes.model";
import { ClientesController } from "./clientes.controller";
import { ClientesService } from "./clientes.service";

@Module({
  imports: [LoggerModule, SequelizeModule.forFeature([Clientes])],
  providers: [{ provide: "ClientesService", useClass: ClientesService }],
  controllers: [ClientesController],
})
export class ClientesModule {}
