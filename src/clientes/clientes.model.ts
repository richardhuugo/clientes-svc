import * as paginate from "sequelize-cursor-pagination";

import { Column, Model, Table, DataType, Index } from "sequelize-typescript";

@Table({
  modelName: "clientes",
  tableName: "Clientes",
})
export class Clientes extends Model<Clientes> {
  @Column({
    primaryKey: true,
    type: DataType.UUID,
    defaultValue: DataType.UUIDV1,
    comment: "The identifier for the empresa record.",
  })
  id: string;

  @Index("nome_completo")
  @Column({
    type: DataType.TEXT,
    comment: "The nome_completo's string.",
  })
  nome_completo: string;

  @Index("dt_nascimento")
  @Column({
    type: DataType.DATE,
    comment: "The dt_nascimento's date.",
  })
  dt_nascimento: string;

  @Index("email")
  @Column({
    type: DataType.TEXT,
    comment: "The email's string.",
  })
  email: string;

  @Index("celular")
  @Column({
    type: DataType.TEXT,
    comment: "The celular's string.",
  })
  celular: string;

  @Index("telefone")
  @Column({
    type: DataType.TEXT,
    comment: "The telefone's string.",
  })
  telefone: string;

  @Column({
    type: DataType.TEXT,
    comment: "The e sexo's string.",
  })
  sexo: string;

  @Column({
    type: DataType.UUID,
    defaultValue: DataType.UUIDV1,
    comment: "The identifier for the empresa record.",
  })
  empresa_id: string;
}

paginate({
  methodName: "findAndPaginate",
  primaryKeyField: "id",
})(Clientes);
